import React, { useState, useEffect } from 'react';

function ConferenceForm() {
    // State variables for each form field
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]); // Assume you want to load these from an API

    // Example of fetching data on component mount
    useEffect(() => {
        const fetchLocations = async () => {
            const url = 'http://localhost:8000/api/locations/';
            try {
                const response = await fetch(url);
                if (response.ok) {
                    const data = await response.json();
                    setLocations(data.locations);
                } else {
                    throw new Error('Failed to load locations');
                }
            } catch (error) {
                console.error('Fetch error:', error);
            }
        };

        fetchLocations();
    }, []);

    // Change handlers for each input
    const handleNameChange = (event) => setName(event.target.value);
    const handleStartDateChange = (event) => setStarts(event.target.value);
    const handleEndDateChange = (event) => setEnds(event.target.value);
    const handleDescriptionChange = (event) => setDescription(event.target.value);
    const handleMaxPresentationsChange = (event) => setMaxPresentations(event.target.value);
    const handleMaxAttendeesChange = (event) => setMaxAttendees(event.target.value);
    const handleLocationChange = (event) => setLocation(event.target.value);

    // Submit handler for the form
    const handleSubmit = async (event) => {
        event.preventDefault();
        const conferenceData = {
            name,
            starts,
            ends,
            description,
            max_presentations: maxPresentations,
            max_attendees: maxAttendees,
            location
        };

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(conferenceData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                const newConference = await response.json();
                console.log(newConference);
                // Reset the form fields
                setName('');
                setStarts('');
                setEnds('');
                setDescription('');
                setMaxPresentations('');
                setMaxAttendees('');
                setLocation('');
            }
            console.log(locations)
        } catch (error) {
            console.error('POST error:', error);
        }
    };

    return (
        <div className="container mt-5">
            <h2>Create New Conference</h2>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label className="form-label">Name</label>
                    <input type="text" className="form-control" value={name} onChange={handleNameChange} required/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Start Date</label>
                    <input type="date" className="form-control" value={starts} onChange={handleStartDateChange} required/>
                </div>
                <div className="mb-3">
                    <label className="form-label">End Date</label>
                    <input type="date" className="form-control" value={ends} onChange={handleEndDateChange} required/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Description</label>
                    <textarea className="form-control" rows="3" value={description} onChange={handleDescriptionChange}></textarea>
                </div>
                <div className="mb-3">
                    <label className="form-label">Max Presentations</label>
                    <input type="number" className="form-control" value={maxPresentations} onChange={handleMaxPresentationsChange} required/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Max Attendees</label>
                    <input type="number" className="form-control" value={maxAttendees} onChange={handleMaxAttendeesChange} required/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Location</label>
                    <select className="form-select" value={location} onChange={handleLocationChange} required>
                        <option value="">Choose a location</option>
                        {locations.map((loc) => (
                            <option key={loc.id} value={loc.id}>{loc.name}</option>
                        ))}
                    </select>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>
    );
}

export default ConferenceForm;
