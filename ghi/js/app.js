function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    const formattedStartDate = new Date(startDate).toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' });
    const formattedEndDate = new Date(endDate).toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' });
    return `
        <div class="card my-3 shadow-sm">
            <img src="${pictureUrl}" class="card-img-top" alt="Image of ${name}">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <p class="card-text">${description}</p>
                <div class="card-footer text-muted">
                    Starts: ${formattedStartDate} - Ends: ${formattedEndDate}
                </div>
            </div>
        </div>
    `;
}
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const cardRow = document.getElementById('card-row');
    let columnIndex = 0;
    const columns = [];
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`Failed to fetch conferences. Server responded with ${response.status}: ${response.statusText}`);
        }
        const data = await response.json();
        data.conferences.forEach(async (conference, index) => {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (!detailResponse.ok) {
                throw new Error(`Failed to fetch details for conference ${conference.name}. Server responded with ${detailResponse.status}: ${detailResponse.statusText}`);
            }
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            const locationName = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDate, endDate, locationName);
            if (!columns[columnIndex]) {
                columns[columnIndex] = document.createElement('div');
                columns[columnIndex].className = 'col-md-4';
                cardRow.appendChild(columns[columnIndex]);
            }
            columns[columnIndex].innerHTML += html;
            columnIndex = (columnIndex + 1) % 3;
        });
    } catch (e) {
        console.error(e);
        cardRow.innerHTML = `
            <div class="alert alert-danger" role="alert">
                Error: ${e.message}
            </div>
        `;
    }
});




// function createdCard(name, description, picture_url) {
//     return `
//         <div class="card">
//             <img src="${picture_url}" class="card-img-top" alt="...">
//             <div class="card-body">
//                 <h5 class="card-title">${name}</h5>
//                 <p class="card-text">${description}</p>
//             </div>
//         </div>
//     `
// }


// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         throw new Error('Failed to fetch conferences');
//       } else {
//         const data = await response.json();

//         for (let conference of data.conferences) {
//           const detailUrl = `http://localhost:8000${conference.href}`;
//           const detailResponse = await fetch(detailUrl);
//           if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             const name = details.conference.name;
//             const description = details.conference.description;
//             const pictureUrl = details.conference.location.picture_url;
//             const html = createdCard(name, description, pictureUrl);
//             const column = document.querySelector('.col');
//             column.innerHTML += html;
//           }
//         }

//       }
//     } catch (e) {
//         console.error('Error:', e);
//         document.querySelector('.card-body').innerHTML = '<p>Error loading conference details.</p>';
//     }

//   });

// window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/';
//     try {
//         const response = await fetch(url);
//         if (!response.ok) {
//             throw new Error('Failed to fetch conferences');
//         } else {
//             const data = await response.json();
//             const conference = data.conferences[1];
//             const nameTag = document.querySelector('.card-title');
//             nameTag.innerHTML = conference.name;
//             const detailUrl = `http://localhost:8000${conference.href}`;
//             const detailResponse = await fetch(detailUrl);
//             if (!detailResponse.ok) {
//                 throw new Error('Failed to fetch conference details');
//             }
//             const details = await detailResponse.json();
//             const confDetails = details.conference;
//             const descriptionTag = document.querySelector('.card-text');
//             descriptionTag.innerHTML = confDetails.description;
//             const imageTag = document.querySelector('.card-img-top');
//             if (details.conference.location.picture_url) {
//                 imageTag.src = details.conference.location.picture_url;
//             } else {
//                 console.log('No image URL provided in the details');
//             }
//         }
//     } catch (e) {
//         console.error('Error:', e);
//         document.querySelector('.card-body').innerHTML = '<p>Error loading conference details.</p>';
//     }
// });
