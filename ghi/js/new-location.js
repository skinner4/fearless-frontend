
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    try {
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Failed to fetch states');
    }
        const data = await response.json();
    const selectTag = document.getElementById('state');
    selectTag.innerHTML = '';
    const defaultOption = document.createElement('option');
    defaultOption.textContent = 'Select a state';
    defaultOption.value = '';
    selectTag.appendChild(defaultOption);
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', event => {
    event.preventDefault();
    console.log('need to submit the form data');
});
    for (let state of data.states) {
        const option = document.createElement('option');
        option.value = state.abbreviation;
        option.textContent = state.name;
        selectTag.appendChild(option);
    }
    } catch (error) {
        console.error('Error:', error.message);
    }
});
document.getElementById('create-location-form').addEventListener('submit', async (event) => {
    event.preventDefault();
    const name = document.getElementById('name').value;
    const roomCount = document.getElementById('room_count').value;
    const city = document.getElementById('city').value;
    const state = document.getElementById('state').value;
    const locationData = {
        name,
        room_count: Number(roomCount),
        city,
        state,
    };
    try {
        const response = await fetch('http://localhost:8000/api/locations/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(locationData)
    });
        if (!response.ok) {
        throw new Error('Failed to create location');
    }
        const result = await response.json();
        console.log('Location created:', result);
    } catch (error) {
        console.error('Error:', error.message);
    }
    });
