

document.addEventListener('DOMContentLoaded', async () => {
    const form = document.getElementById('new-conference-form');
    form.addEventListener('submit', async function (event) {
        event.preventDefault();
        const formData = {
            name: document.getElementById('name').value,
            starts: document.getElementById('starts').value,
            ends: document.getElementById('ends').value,
            description: document.getElementById('description').value,
            max_presentations: parseInt(document.getElementById('max_presentations').value, 10),
            max_attendees: parseInt(document.getElementById('max_attendees').value, 10),
            location: parseInt(document.getElementById('location').value, 10)
        };
        try {
            const response = await fetch('http://localhost:8000/api/conferences/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Failed to create conference');
            }
            const result = await response.json();
            console.log('Conference created:', result);
            form.reset();
        } catch (error) {
            console.error('Error:', error.message);
        }
    });
    try {
        const response = await fetch('http://localhost:8000/api/locations/');
        if (!response.ok) {
            throw new Error('Failed to fetch locations');
        }
        const data = await response.json();
        const locationSelect = document.getElementById('location');
        data.locations.map(location => {
            const option = document.createElement('option');
            option.value = location.id;
            option.textContent = location.name;
            locationSelect.appendChild(option);
    });
    }
    catch (error) {
        console.error('Error:', error.message);
    }
});






// document.addEventListener('DOMContentLoaded', async () => {
//     const locationsUrl = 'http://localhost:8000/api/locations/';
//     const locationSelect = document.getElementById('location');

//     try {
//         const locationResponse = await fetch(locationsUrl);
//         if (locationResponse.ok) {
//             const locations = await locationResponse.json();
//             for (let location of locations) {
//                 const option = document.createElement('option');
//                 option.value = location.id;
//                 option.textContent = location.name;
//                 locationSelect.appendChild(option);
//             };
//         }
//     } catch (error) {
//         console.error('Failed to load locations:', error);
//     }

//     const form = document.getElementById('conference-form');
//     form.addEventListener('submit', async (event) => {
//         event.preventDefault();

//         const formData = new FormData(form);
//         const jsonData = JSON.stringify(Object.fromEntries(formData));
//         const postUrl = 'http://localhost:8000/api/conferences/';

//         const fetchConfig = {
//             method: 'POST',
//             headers: {'Content-Type': 'application/json'},
//             body: json
//         };

//         try {
//             const postResponse = await fetch(postUrl, fetchConfig);
//             if (postResponse.ok) {
//                 const newLocation = await postResponse.json();
//                 console.log('New conference created:', newLocation);
//                 form.reset(); // Clear the form after successful submission
//             } else {
//                 console.error('Failed to create conference:', await postResponse.text());
//             }
//         } catch (error) {
//             console.error('Error sending conference data:', error);
//         }
//     });
// });

